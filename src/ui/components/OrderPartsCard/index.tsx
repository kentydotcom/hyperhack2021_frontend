import React from 'react';
import './style.css';
import {Button, Card, Container, Form, Nav, Navbar} from "react-bootstrap";
import Image from "react-bootstrap/Image";
import car_logo from "../../assets/compact_car_logo.png";

class OrderPartsCard extends React.Component {
    render() {
        return (
            <div>
                <Card border="info" style={{ width: '24rem', height: '36rem' }}>
                    <Card.Header>
                       <h3><Image src={car_logo} id="compact_car_logo"/>OEM</h3>
                    </Card.Header>
                    <Card.Body>
                        <Card.Title>
                            <h4>Order Car Parts</h4>
                        </Card.Title>
                        <br/>
                        <Card.Text>
                            <p className="OrderPartsText">
                                <Container>
                                    <Form.Group controlId="formOrderParts">
                                        <Form.Label>SKU Number </Form.Label>
                                        <Form.Control type="text" placeholder="Enter SKU#" text-align="center"/>
                                    </Form.Group>
                                </Container>
                                <br/>
                                <Container>
                                    <Form.Group controlId="formQuantity">
                                        <Form.Label>Quantity </Form.Label>
                                        <Form.Control type="text" placeholder="Enter Quantity"/>
                                    </Form.Group>
                                </Container>
                            </p>
                            <br/>
                            <Button
                                variant="primary"
                                type="submit"
                                size="lg"
                                active
                                href="selectSupplier"
                            >
                                Submit
                            </Button>
                        </Card.Text>
                    </Card.Body>
                </Card>
            </div>
        );
    }
}

export default OrderPartsCard;
