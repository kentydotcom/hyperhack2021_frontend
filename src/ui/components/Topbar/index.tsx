import React from 'react';
import './style.css';
import LogoutButton from "../LogoutButton";
import logo1 from "../../assets/logo1.png";
import {Container, Nav, Navbar} from "react-bootstrap";

class Topbar extends React.Component {
    render() {
        return (
            <div>
                <Navbar
                    bg="primary"
                    variant="dark"
                    expand="lg"
                >
                    <Navbar.Brand>
                        <img
                            src={logo1}
                            width="243"
                            height="78"
                            className="d-inline-block align-top"
                            alt="Team PHDDI logo"
                        />
                    </Navbar.Brand>
                    <Nav>
                        <Nav.Link
                            className="about-link"
                            href="About">
                            About
                        </Nav.Link>
                        <Nav.Link
                            className="home-link"
                            href="Home">
                            Home
                        </Nav.Link>
                        <Nav.Link
                            className="orderParts-link"
                            href="orderParts">
                            OrderParts
                        </Nav.Link>
                    </Nav>
                        <LogoutButton />
                </Navbar>
            </div>
        );
    }
}

export default Topbar;
