import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import React from 'react';
import {Button} from "react-bootstrap";

class ConfirmAlert extends React.Component {
    submit = () => {
        confirmAlert({
            title: 'Confirm to submit',
            message: 'Are you sure to do this.',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => alert('Selected Yes!'),
                },
                {
                    label: 'No',
                    onClick: () => alert('Selected No!')
                }
            ]
        });
    };

    render() {
        return (
            <div className='container'>
                <Button onClick={this.submit}>Submit chosen Supplier</Button>
            </div>
        );
    }
}

export default ConfirmAlert;
