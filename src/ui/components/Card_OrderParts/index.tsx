import React from 'react';
import './style.css';
import {Card, Container, Nav, Navbar} from "react-bootstrap";
import FooterCardNavigation from "../OrderPartsCard";

class FooterBar extends React.Component {
    render() {
        return (
            <Card.Footer>
                <FooterCardNavigation />
            </Card.Footer>
        );
    }
}

export default FooterBar;
